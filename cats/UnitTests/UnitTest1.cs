using cats.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            CatsController ct = new CatsController();
            Assert.AreEqual(9, ct.GetAnswer_Good(4, 5));
        }


        [TestMethod]
        public void TestMethod2()
        {
            CatsController ct = new CatsController();
            if ((new Random()).Next(0, 100) > 25)
                Assert.AreEqual(4, ct.GetAnswer_Wait(4));
            else
                Assert.AreEqual(10, ct.GetAnswer_Wait(4));
        }


        [TestMethod]
        public void TestMethod3()
        {
            CatsController ct = new CatsController();
            ct.GetAnswer_Bad(4);
        }
    }
}
