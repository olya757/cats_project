﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace cats.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatsController : ControllerBase
    {
        public CatsController() { 
        }

        [HttpGet]
        public ActionResult<string> Get()
        {
            return "All is OK";
        }

        [HttpGet("{elem1}/{elem2}")]
        public int GetAnswer_Good(int elem1, int elem2)
        {
            return elem1 + elem2;
        }

        [HttpGet("wait_answer/{elem1}")]
        public int GetAnswer_Wait(int elem1)
        {
            Thread.Sleep(5000);
            return Math.Abs(elem1);
        }


        [HttpGet("bad_answer/{elem1}")]
        public ActionResult GetAnswer_Bad(int elem1)
        {
            return NotFound();
        }
    }
}